/*
setTimeout(() => {
    console.log('Hello');
}, 1000);

const promise = new Promise(pCallback);

function pCallback(resolve, reject) {
    setTimeout(() => {
        resolve(1);
    }, 2000);
}
// .then(), .catch(), .finally()

promise
.then(value => console.log('THEN: ', value))
.catch(err => console.log('CATCH: ', err))
.finally(() => {console.log('Promis end')}); 
*/

// ------------------------------------
/*
const channelsData = fetch('https://api.persik.by/v2/content/channels', {method: 'GET',});

channelsData
.then(response => response.json())
.then(data => console.log(data))
.catch(err => console.log(err))
.finally(() => console.log('Promise end'));
*/
// ---------- new lesson -------------------


let userList = [];
let filteredUsers = [];
let sortWay = 'desc'; //descending
const container = document.querySelector('.container');
const countryFilter = document.querySelector('.country-filter');
const sortAgeBtn = document.querySelector('.age-sort');
const sortAZ = document.querySelector('.name-sort-asc');
const sortZA = document.querySelector('.name-sort-desc');

countryFilter.addEventListener('change', changeCountry);
sortAgeBtn.addEventListener('click', ageSort);
sortAZ.addEventListener('click', nameAZSort);
sortZA.addEventListener('click', nameZASort);

function getUserList() {
    showLoader();
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://randomuser.me/api?results=100');
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    const data = JSON.parse(xhr.responseText);
                    resolve(createData(data.results));
                } else {
                    reject('No data');
                }
            }
        }
        xhr.send();
    });
}

function createData(data) {
    return userLIst = data.map(user => {
        return {
            name: user.name.first + ' ' + user.name.last,
            age: user.dob.age,
            location: user.location.country,
        }
    });
}

getUserList()
    .then(users => {
        userList = users;
        filteredUsers = users;
        drawUsers(users);
        const countries = getUniqueCountries();
        showCountries(countries);
    })
    .catch(err => console.log(err))
    .finally(() => hideLoader());

function showLoader() {
    document.querySelector('.loader').style.display = 'flex';
}

function hideLoader() {
    document.querySelector('.loader').style.display = 'none';
}

function createUserCard(user) {
    const card = document.createElement('div');
    card.classList.add('card');

    const nameSpan = document.createElement('span');
    nameSpan.classList.add('name');
    nameSpan.innerText = user.name;

    const ageSpan = document.createElement('span');
    ageSpan.classList.add('age');
    ageSpan.innerText = user.age;

    const locationSpan = document.createElement('span');
    locationSpan.classList.add('location');
    locationSpan.innerText = user.location;

    card.appendChild(nameSpan);
    card.appendChild(ageSpan);
    card.appendChild(locationSpan);

    return card;
}

function drawUsers(users) {
    container.innerHTML = '';
    users.forEach(item => {
        const userCard = createUserCard(item);
        container.appendChild(userCard);
    })
}

function getUniqueCountries() {
    const countries = userList.map(user => user.location);
    //console.log(Array.from(new Set(countries)));
    const unique = countries.filter((item, index, self) => self.indexOf(item) === index);
    unique.unshift('All countries');
    return unique;
}

function createFilterOption(country) {
    const option = document.createElement('option');
    option.value = country;
    option.innerText = country;
    return option;
}

function showCountries(countries) {
    countries.forEach(item => {
        const option = createFilterOption(item);
        countryFilter.appendChild(option);
    });
}

function changeCountry(event) {
    const selectedCountry = event.target.value;
    filteredUsers = userList;
    if (selectedCountry !== 'All countries') {
        filteredUsers = userList.filter(item => item.location === selectedCountry);
    }
    drawUsers(filteredUsers);
}

function ageSort() {
    sortWay = sortWay === 'asc' ? 'desc' : 'asc'; 

    filteredUsers.sort((a, b) => {
        if (sortWay === 'asc') {
            return a.age - b.age;
        }

        return b.age - a.age;
    });
    drawUsers(filteredUsers);
}

function nameAZSort() {
    filteredUsers.sort((left, right) => {
        const nameLeft = left.name.toLowerCase();
        const nameRight = right.name.toLowerCase();
        return nameLeft.localeCompare(nameRight);
    });

    drawUsers(filteredUsers);
}

function nameZASort() {
    filteredUsers.sort((left, right) => {
        const nameLeft = left.name.toLowerCase();
        const nameRight = right.name.toLowerCase();
        return nameRight.localeCompare(nameLeft);
    });

    drawUsers(filteredUsers);
}